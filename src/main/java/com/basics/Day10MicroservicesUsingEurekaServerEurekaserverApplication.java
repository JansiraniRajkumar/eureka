package com.basics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Day10MicroservicesUsingEurekaServerEurekaserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day10MicroservicesUsingEurekaServerEurekaserverApplication.class, args);
	}

}
